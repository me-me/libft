NAME = libft.a

CFLAG = gcc -Wall -Werror -Wextra -c

INC = libft.h

SRCS = ft_putchar.c ft_memset.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c ft_memchr.c ft_memcmp.c ft_strlen.c ft_strdup.c ft_strcpy.c ft_strncpy.c ft_strcat.c ft_strncat.c ft_strcmp.c ft_strncmp.c ft_strlcat.c ft_atoi.c ft_strrchr.c ft_strchr.c ft_strstr.c ft_strnstr.c ft_isalpha.c ft_isdigit.c ft_isalnum.c ft_isascii.c ft_isprint.c ft_toupper.c ft_tolower.c ft_putstr.c ft_memalloc.c ft_memdel.c ft_strnew.c ft_strdel.c ft_strclr.c ft_striter.c ft_striteri.c ft_strmap.c ft_strmapi.c ft_strequ.c ft_strnequ.c ft_strsub.c ft_strjoin.c ft_strtrim.c ft_putnbr.c ft_putendl.c ft_putchar_fd.c ft_putstr_fd.c ft_putendl_fd.c ft_putnbr_fd.c ft_strsplit.c ft_itoa.c ft_lstnew.c ft_lstadd.c ft_lstdel.c ft_lstiter.c ft_lstmap.c ft_ftoa.c ft_lstpushback.c ft_lstshift.c ft_nblen.c ft_nblenbase.c ft_pow.c ft_printbits.c ft_strjoinfree.c ft_swapbits.c ft_uimtoa_base.c ft_imtoa_base.c ft_itoa_base.c get_next_line.c ft_lstaddback.c ft_sqrt.c

RM = rm -f

OBJ = $(SRCS:.c=.o)

all: $(NAME)

$(NAME):
		@$(CFLAG) $(SRCS) -I $(INC)
		@ar rc $(NAME) $(OBJ)
		@ranlib $(NAME)
		@printf '\033[32m----> Libft compiled \033[33m    [OK]\n'

clean:
		@$(RM) $(OBJ)
		@printf '\033[32m----> Libft clean \033[33m       [OK]\n'

fclean: clean
		@$(RM) $(NAME)
		@printf '\033[32m----> Libft full clean \033[33m  [OK]\n'

re: fclean all
