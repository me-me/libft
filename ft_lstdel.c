/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 08:19:01 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/19 08:19:33 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*victim;

	while (*alst)
	{
		victim = *alst;
		*alst = (*alst)->next;
		del(victim->content, victim->content_size);
		free(victim);
	}
	*alst = NULL;
}
