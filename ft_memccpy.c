/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 16:37:07 by abnaceur          #+#    #+#             */
/*   Updated: 2016/12/01 17:02:42 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, void const *src, int c, size_t n)
{
	char *tdst;
	char *tsrc;

	tdst = (char*)dst;
	tsrc = (char*)src;
	while (n > 0 && *tsrc != c)
	{
		n--;
		*tdst++ = *tsrc++;
	}
	if (n > 0)
	{
		*tdst++ = *tsrc++;
		return ((void*)tdst);
	}
	else
		return (NULL);
}
