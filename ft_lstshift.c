/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstshift.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 07:46:58 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 07:47:04 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lstshift(t_list **alst, void (*del)(void **))
{
	t_list *lst;

	if (!alst || !del || !*alst)
		return ;
	lst = (*alst)->next;
	del(&((*alst)->content));
	del((void **)alst);
	*alst = lst;
}
