/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nblenbase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 07:47:46 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 07:50:53 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nblenbase(uintmax_t n, unsigned int base)
{
	if (n < base)
		return (1);
	return (1 + ft_nblenbase(n / base, base));
}
