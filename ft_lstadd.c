/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 08:15:47 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/19 08:16:55 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstadd(t_list **alst, t_list *nw)
{
	if (*alst == NULL)
		*alst = ft_lstnew(NULL, 0);
	if (*alst && nw)
	{
		nw->next = *alst;
		*alst = nw;
	}
}
