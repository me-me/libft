/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushback.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abnaceur <abnaceur@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 07:46:24 by abnaceur          #+#    #+#             */
/*   Updated: 2017/04/23 07:46:30 by abnaceur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lstpushback(t_list **alst, t_list *elem)
{
	t_list *lst;

	if (!alst || !elem)
		return ;
	if (!*alst)
	{
		*alst = elem;
		return ;
	}
	lst = *alst;
	while (lst->next)
		lst = lst->next;
	lst->next = elem;
}
